﻿using UnityEngine;
using System.Collections;

public class Bull : MonoBehaviour {
	public float MAX_SPEED = 0.2f;
	public float ACC_SPEED = 2;
	public float CHARGE_SPEED = 2;
	public float MAX_CHARGE_SPEED = 0.2f;
	public float JUMP_SPEED = 0.5f;
	public float DRAG_AMOUNT = 3;
	public float JUMP_FLAG_TIME=0.4f;
	public float CHARGE_TIME = 2.0f;

	public State state = State.WAIT;
	Direction dir = Direction.LEFT;

	public ParticleSystem warnedPS;
	public ParticleSystem charingPS;

	float timer = 0;
	// Use this for initialization
	void Start () {
		startWaiting ();
	}
	
	// Update is called once per frame
	void Update () {
		float t = Time.deltaTime;

		switch(state) {
		case State.WAIT:
			timer -=t;
			if (timer < 0)
				startWalking();
			break;
		case State.WALK:
			timer -=t;
			if (timer < 0)
				startWaiting();
			float movementDirection = 0;
			if (dir == Direction.LEFT)
				movementDirection = -1;
			else 
				movementDirection = 1;

			if (movementDirection > 0)
				transform.localScale = new Vector3(-3,2);
			else if (movementDirection < 0)
				transform.localScale = new Vector2(3,2);

			rigidbody2D.AddForce(new Vector2(movementDirection * ACC_SPEED,0));
			if (Mathf.Abs (movementDirection) == 0 && Mathf.Abs(rigidbody2D.velocity.x) > 0) {
				rigidbody2D.AddForce (new Vector2(-(rigidbody2D.velocity.x/Mathf.Abs(rigidbody2D.velocity.x)*DRAG_AMOUNT),0));
			}
			rigidbody2D.velocity = new Vector2(Mathf.Clamp(rigidbody2D.velocity.x,-MAX_SPEED,MAX_SPEED),rigidbody2D.velocity.y);
			break;
		case State.WARNED:
			timer -=t;
			if (timer < 0)
				startCharging();
			break;
		case State.CHARGE:
			timer -=t;
			if (timer < 0)
				startWaiting();
			float movDir = 0;
			if (dir == Direction.LEFT)
				movDir = -1;
			else 
				movDir = 1;
			rigidbody2D.AddForce(new Vector2(movDir * ACC_SPEED,0));
			if (Mathf.Abs (movDir) == 0 && Mathf.Abs(rigidbody2D.velocity.x) > 0) {
				rigidbody2D.AddForce (new Vector2(-(rigidbody2D.velocity.x/Mathf.Abs(rigidbody2D.velocity.x)*DRAG_AMOUNT),0));
			}
			rigidbody2D.velocity = new Vector2(Mathf.Clamp(rigidbody2D.velocity.x,-MAX_CHARGE_SPEED,MAX_CHARGE_SPEED),rigidbody2D.velocity.y);
			break;
		}

		//check for player nearby
		float mov = 0;
		if (dir == Direction.LEFT)
			mov = -1;
		else 
			mov = 1;
		Debug.DrawLine (transform.position + Vector3.up + new Vector3(mov*2,0,0), transform.position + Vector3.up + new Vector3(mov*6,0,0), Color.red,0.00001f,false);
		bool raycast = Physics2D.Linecast((Vector2)transform.position + Vector2.up + new Vector2(mov*2,0), (Vector2)transform.position + Vector2.up + new Vector2(mov*6,0),(1 << LayerMask.NameToLayer("Player")) | (1 << LayerMask.NameToLayer("Enemies")));
		if (state != State.CHARGE && state != State.WARNED && raycast)
		{
			startWarned();
			if (mov > 0)
				transform.localScale = new Vector3(-3,2);
			else if (mov < 0)
				transform.localScale = new Vector2(3,2);
		}
		else if (state == State.CHARGE && raycast)
			timer = CHARGE_TIME;
		else if (state == State.WARNED && !raycast)
			startWaiting ();

	}
	public void startWaiting(){
		charingPS.Stop ();
		warnedPS.Stop ();
		timer = Random.Range (2.0f,6.0f);
		state = State.WAIT;
	}
	public void startWalking()
	{
		timer = Random.Range (1f,1.5f);
		dir=(Direction)Random.Range (0,2);
		state = State.WALK;
	}
	public void startWarned()
	{
		timer = Random.Range (3f,5f);
		//dir=(Direction)Random.Range (0,2); //player DIR
		state = State.WARNED;
		warnedPS.Play();

	}
	public void startCharging()
	{
		timer = CHARGE_TIME;
		warnedPS.Stop ();
		charingPS.Play ();
		state = State.CHARGE;
	}

	public enum State{
		WALK,
		WAIT,
		WARNED,
		CHARGE
	}
	public enum Direction { LEFT, RIGHT }
	
}
