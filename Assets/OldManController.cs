﻿using UnityEngine;
using System.Collections;

public class OldManController : MonoBehaviour {

	public Transform buttonY;
	bool buttonActive = false;
	bool buttonSized = false;
	float buttonSizedTime=0;
	public Transform player;
	string[] lines;
	int currentLine = 0;
	bool dialogShown = false;
	bool dialogActive=false;
	bool endingDialog=false;
	float endDialogTime = 0;
	GameObject dialogBox;
	public ParticleSystem psLight;
	public ParticleSystem psDark;
	public AudioClip SoundSpeech;
	public AudioClip SoundDissappear;

	void Start()
	{
		player = GameObject.Find ("Character").transform;
	}

	void Update () {
		if (!dialogActive)
		{
			if (Vector2.Distance (player.position,transform.position) < 5)
				buttonActive=true;
			else
				buttonActive=false;

			if (buttonActive && !buttonSized)
			{
				buttonY.localScale = Vector2.MoveTowards (buttonY.localScale, Vector2.one * 1.6f, Time.deltaTime*6);
				if (buttonY.localScale.x == 1.6f){
					buttonSized=true;
					buttonSizedTime=Time.realtimeSinceStartup;
				}
			}
			else if (buttonActive && buttonSized)
			{
				buttonY.localScale = Vector2.one *1.6f* (Mathf.Sin ((Time.realtimeSinceStartup-buttonSizedTime)*5)*0.2f+1);
			}
			else if (!buttonActive && buttonSized)
			{
				buttonY.localScale = Vector2.MoveTowards (buttonY.localScale, Vector2.zero, Time.deltaTime*6);
				if ((Vector2)buttonY.localScale == Vector2.zero) {
					buttonSized=false;
				}
			}
		}
		else 
		{
			buttonSized=false;
			buttonActive=false;
			buttonY.localScale=Vector2.zero;
			if (!endingDialog)
			{
				if (!dialogShown)
				{
					drawDialogBox(lines[currentLine]);
					dialogShown = true;
				}
			}
			else {
				if(endDialogTime==0) {
					GameObject.Instantiate (psLight,transform.position+new Vector3(0,0.3f,-0.2f),Quaternion.Euler (-90,0,0));
					GameObject.Instantiate (psDark,transform.position+new Vector3(0,0.3f,-0.2f),Quaternion.Euler (-90,0,0));
					Camera.main.audio.PlayOneShot(SoundDissappear);
				}
				endDialogTime += Time.deltaTime;
				if (endDialogTime > 1f) {
					transform.Translate (8,0,0);
					dialogActive=false;
					endingDialog=false;
					dialogShown=false;
					endDialogTime=0;
				}
			}
		}
	}

	public void drawDialogBox(string s)
	{
		if (dialogBox == null)
			dialogBox = (GameObject)GameObject.Instantiate ((GameObject)Resources.Load ("GUI/dialogBox"));
		dialogBox.transform.position = (Vector2)transform.position + new Vector2(0,3);
		DialogBoxController dialogBoxC = dialogBox.GetComponent<DialogBoxController>();
		dialogBoxC.text=s;
	}
	// Update is called once per frame
	public void Activate(string s)
	{
		if (!dialogActive) {
			Debug.Log ("ACTIVATE");
			lines = s.Split('%');
			dialogActive=true;
			currentLine=0;
			Camera.main.audio.PlayOneShot(SoundSpeech);
		}
	}
	public bool Talk(){
		
		Debug.Log ("TALK");
		if (currentLine+1 == lines.Length)
		{
			GameObject.Destroy (dialogBox);
			endingDialog=true;
			return true;
		}
		currentLine+=1;
		dialogShown = false;
		return false;
		
	}
}
