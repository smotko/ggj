﻿using UnityEngine;
using System.Collections;

public class DialogBoxController : MonoBehaviour {
	
	public Transform topLeft;
	public Transform topRight;
	public Transform bottomLeft;
	public Transform bottomRight;
	public string text;
	public TextMesh textMesh;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		textMesh.text= text;
		float width = textMesh.renderer.bounds.size.x + 0.5f;
		float height = textMesh.renderer.bounds.size.y ;

		topLeft.position = transform.position + new Vector3(-width, height,0);
		topRight.position = transform.position + new Vector3(width, height,0);
		bottomLeft.position = transform.position + new Vector3(-width, -height,0);
		bottomRight.position = transform.position + new Vector3(width, -height,0);
	}
}
