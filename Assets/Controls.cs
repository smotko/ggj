﻿using UnityEngine;
using System.Collections;

public class Controls : MonoBehaviour {

	public float speed = 0.01f;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		if(Input.GetAxis("Horizontal") != 0){
			transform.Translate(speed*(Input.GetAxis("Horizontal")/Mathf.Abs(Input.GetAxis("Horizontal"))), 0, 0);
		}
		else{
			transform.Translate(0,0,0);
		}


	}
}
