﻿Shader "GJ/UnlitWithTintZWriteOn" {
Properties {
    _MainTex ("Texture", 2D) = "white" {}
    _Color ("Tint", Color) = (0.5,0.5,0.5,0.5)
}
SubShader {
	Tags {"Queue" = "Transparent-1" }
    Pass {
	Blend SrcAlpha OneMinusSrcAlpha
	ZWrite On
	ZTest On
	Cull Off
	CGPROGRAM
	#pragma vertex vert
	#pragma fragment frag
	#include "UnityCG.cginc"
	
	fixed4 _Color;
	sampler2D _MainTex;
	float4 _MainTex_ST;
	
	struct v2f {
	    float4 pos : SV_POSITION;
	    float4 tex : TEXCOORD0;
	};

	v2f vert (appdata_base v)
	{
	    v2f o;
	    o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
	    o.tex = v.texcoord ;
	    return o;
	}

	half4 frag (v2f i) : COLOR
	{
		float4 tex = tex2D(_MainTex,i.tex.xy * _MainTex_ST.xy);
	    return tex*_Color;
	}
	ENDCG

    }
}
Fallback "VertexLit"
} 