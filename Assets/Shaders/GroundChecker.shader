﻿Shader "GJ/GroundChecker" {
Properties {
    _Color ("Main Color", Color) = (0.5,0.5,0.5,0.5)
}
SubShader {
    Pass {

	CGPROGRAM
	#pragma vertex vert
	#pragma fragment frag
	#include "UnityCG.cginc"
	
	fixed4 _Color;
	
	struct v2f {
	    float4 pos : SV_POSITION;
	    float2 tex : TEXCOORD0;
	};

	v2f vert (appdata_base v)
	{
	    v2f o;
	    o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
	    float3 worldPos = mul (_Object2World, v.vertex).xyz;
	    o.tex = float2(worldPos.x, worldPos.y);
	    return o;
	}

	half4 frag (v2f i) : COLOR
	{
	    return _Color + fixed4(0.1,0.1,0.1,1) * fmod(floor(i.tex.x),2) * fmod(floor(i.tex.y),2);
	}
	ENDCG

    }
}
Fallback "VertexLit"
} 