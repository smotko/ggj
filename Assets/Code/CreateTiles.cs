﻿using UnityEngine;
using System.Collections;

public class CreateTiles : MonoBehaviour {

	public int SIZE_X = 100;
	public int SIZE_Y = 40;

	public GameObject[][] types;

	public enum TYPE { GROUND, AIR, FILLED };
	public TYPE type;
	public bool enable;
	public GameObject groundObject;
	public GameObject skyObject;
	public string spriteNames = "TileSprite";

	private LevelBase[] levels = {new Level1(), new Level2(), new Level3(), new Level4(), new Level5()}; 
	private int currentLevel = -1;

	void Start () {

		types = new GameObject[SIZE_X][];

		if(!enable) return;
		for(int i = 0; i < SIZE_X; i++){
			types[i] = new GameObject[SIZE_Y];
		}
		nextSet();

	}
	private float setTimeDiff = -10;
	public void nextSet(){

		if(Time.fixedTime < setTimeDiff + 1){
			return;
		}
		setTimeDiff = Time.fixedTime;
		if(++currentLevel == levels.Length){

			currentLevel = 0;
		}
		levels[currentLevel].generateLevel(types);
	}

	void FixedUpdate(){
		levels[currentLevel].FixedUpdate();
	}
	// Update is called once per frame
	void Update () {
	
	}
}
