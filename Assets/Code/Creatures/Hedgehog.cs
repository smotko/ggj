﻿using UnityEngine;
using System.Collections;

public class Hedgehog : MonoBehaviour {
	public float MAX_SPEED = 0.2f;
	public float ACC_SPEED = 2;
	public float DRAG_AMOUNT = 3;
	
	public State state = State.WAIT;
	Direction dir = Direction.LEFT;
	
	float timer = 0;
	// Use this for initialization
	void Start () {
		startWaiting ();
	}
	
	// Update is called once per frame
	void Update () {
		float t = Time.deltaTime;
		
		switch(state) {
		case State.WAIT:
			timer -=t;
			if (timer < 0)
				startWalking();
			break;
		case State.WALK:
			timer -=t;
			if (timer < 0)
				startWaiting();
			float movementDirection = 0;
			if (dir == Direction.LEFT)
				movementDirection = -1;
			else 
				movementDirection = 1;
			
			if (movementDirection > 0)
				transform.localScale = new Vector3(1.2f,1.2f);
			else if (movementDirection < 0)
				transform.localScale = new Vector2(-1.2f,1.2f);
			
			rigidbody2D.AddForce(new Vector2(movementDirection * ACC_SPEED,0));
			if (Mathf.Abs (movementDirection) == 0 && Mathf.Abs(rigidbody2D.velocity.x) > 0) {
				rigidbody2D.AddForce (new Vector2(-(rigidbody2D.velocity.x/Mathf.Abs(rigidbody2D.velocity.x)*DRAG_AMOUNT),0));
			}
			rigidbody2D.velocity = new Vector2(Mathf.Clamp(rigidbody2D.velocity.x,-MAX_SPEED,MAX_SPEED),rigidbody2D.velocity.y);
			break;
		}
		
	}
	public void startWaiting(){
		timer = Random.Range (1.0f,3.0f);
		state = State.WAIT;
	}
	public void startWalking()
	{
		timer = Random.Range (3f,3.5f);
		dir=(Direction)Random.Range (0,2);
		state = State.WALK;
	}
	
	public enum State{
		WALK,
		WAIT
	}
	public enum Direction { LEFT, RIGHT }
	
}
