﻿using UnityEngine;
using System.Collections;

public class Level5 : LevelBase {

	private Transform player;
	private GameObject[][] world;

	public override void generateLevel(GameObject[][] world){
		
		foreach(GameObject gg in GameObject.FindGameObjectsWithTag("burning")){
			int i = (int)(gg.transform.position.x+0.5f), j = (int)(gg.transform.position.y+0.5f);
			GameObject g = getObject("Block/burned",  i,j);
			g.transform.position = gg.transform.position;
			GameObject.Destroy(gg);
			world[i][j] = g;
		}

		this.world = world;
		player = GameObject.FindGameObjectWithTag("Player").transform;
		
		for(int i = 20; i < world.Length-20; i += Random.Range(3, 8)){
			
			GameObject hog = getObject("Creatures/hedgehog",  i,19);
			world[i][19] = hog;
			
		}
	}
	
	public override void FixedUpdate(){

		for(int i = 0; i < world.Length; i++){
			GameObject g = world[i][19];
			if(g == null) continue;
			if(Mathf.Abs (g.transform.position.x - player.position.x + 15) < 10){
				Rigidbody2D rb = g.GetComponent<Rigidbody2D>();
				rb.gravityScale = 1.8f;
			}
		}
	}
}
