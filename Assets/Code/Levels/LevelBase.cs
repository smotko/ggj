﻿using UnityEngine;
using System.Collections;

public class LevelBase {

	//protected Transform parent;

	void Start(){
		//parent = GameObject.Find("TileCreator").transform;
		//player = GameObject.FindGameObjectWithTag("Player").transform;
	}

	public virtual void generateLevel(GameObject[][] world){}

	protected GameObject getStone(float i, float j){
		return getObject("Block/stone", i, j);
	}
	protected GameObject getGround(float i, float j){
		return getObject("Block/ground", i, j);
	}
	protected GameObject getGrass(float i, float j){
		return getObject("Block/grass", i, j);	
	}
	protected GameObject getObject(string path, float i, float j){
		GameObject go = GameObject.Instantiate((GameObject)Resources.Load(path), new Vector3(i-0.5f, j-0.5f, 0), new Quaternion(0,0,0,0)) as GameObject;
		//go.transform.parent = parent;
		return go;
	}
	protected virtual void resetWorld(GameObject[][] world){
		for(int i = 0; i < world.Length; i++){
			for(int j = 0; j < world[i].Length; j++){
				if(world[i][j] != null){
					GameObject.Destroy(world[i][j]);
					world[i][j] = null;
				}
			}
		}
	}
	public virtual void FixedUpdate(){}
}
