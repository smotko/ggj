﻿using UnityEngine;
using System.Collections;

public class Level4 : LevelBase {
	
	public override void generateLevel(GameObject[][] world){

		foreach(GameObject gg in GameObject.FindGameObjectsWithTag("groundGrass")){
			if(Random.Range(0f, 1f) < 0.1){

				int i = (int)(gg.transform.position.x+0.5f), j = (int)(gg.transform.position.y+0.5f);
				if(i < 20 || i > 100) continue;
				GameObject g = getObject("Block/burning",  i,j);
				g.transform.position = gg.transform.position;
				GameObject.Destroy(gg);
				world[i][j] = g;
			}
		}
	}
}
