﻿using UnityEngine;
using System.Collections;

public class Level2 : LevelBase {

	int grass_index = 2;
	GameObject[][] world ;

	private Transform player;

	public override void generateLevel(GameObject[][] world){
		this.world = world;
		player = GameObject.FindGameObjectWithTag("Player").transform;

		for(int i = 0; i < world.Length; i++){

			GameObject grass = getGrass(i, 20+Random.Range(0, 1.2f));
			Rigidbody2D rb = grass.GetComponent<Rigidbody2D>();
			rb.gravityScale = 0.0f;
			world[i][grass_index] = grass;

		}
	}

	public override void FixedUpdate(){



		for(int i = 0; i < world.Length; i++){
			GameObject g = world[i][grass_index];
			if(Mathf.Abs (g.transform.position.x - player.position.x + 15) < 10){
				Rigidbody2D rb = g.GetComponent<Rigidbody2D>();
				rb.gravityScale = 1.8f;
			}
		}
	}
}
