﻿using UnityEngine;
using System.Collections;

public class Level1 : LevelBase {

	public override void generateLevel(GameObject[][] world){

		resetWorld(world);

		for(int i = 0; i < world.Length; i++){
			world[i][0] = getGround(i,0);
		}
	}
}
