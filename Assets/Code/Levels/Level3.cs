﻿using UnityEngine;
using System.Collections;

public class Level3 : LevelBase {

	GameObject[][] world; 
	Vector3[][] worldDone;
	int skip = 20, max_height = 10, current_height = 0;
	bool top = true;
	private Transform player;
	
	public override void generateLevel(GameObject[][] world){

		resetWorld(world);

		worldDone = new Vector3[world.Length][];
		player = GameObject.FindGameObjectWithTag("Player").transform;
		this.world = world;
		for(int i = 0; i < world.Length; i++){
			world[i][0] = getObject("Block/groundGrass", i, 0);
		}
		for(int i = skip; i < world.Length - skip; i++){
			worldDone[i] = new Vector3[world[i].Length];

			for(int j = 0; j < current_height && j < max_height - 1; j++){
				world[i][j+1] = (Random.Range(0f,1f) < 0.45f && j == 0) ? getGround(i,j*-1-1) : getStone(i,j*-1-1);
			}

			float r = Random.Range(0f, 1f);
			//if(r < 0.15f){
			//	current_height+=2;
			//}
			if(r<0.35f){
				current_height++;
			}
			else if(r<0.5f){
				current_height--;
			}
			else if(r<0.95f){
				// stay at current height
			}
			else{
				current_height-=2;
			}
			if(world.Length - skip*2 - current_height - i - 3 < 0){
				current_height--;
			
			current_height = Mathf.Clamp(current_height, 0, max_height);
			}
		}

	}

	public override void FixedUpdate(){
		for(int i = skip; i < world.Length - skip; i++){
			if(player == null) continue;

			if(Mathf.Abs (world[i][0].transform.position.x - player.position.x - 5) < 10){
				int j = -1;

				int amount = -1;
				while(j < world[i].Length && world[i][++j] != null){
					amount++;
				}
				j = -1;
				while(world[i][++j] != null){
					Vector3 t = world[i][j].transform.position;
					if(worldDone[i][j].y == 0){
						worldDone[i][j] = new Vector3(t.x, t.y+amount, t.z);
					}
					world[i][j].transform.position = Vector3.MoveTowards(t, worldDone[i][j], Time.deltaTime*19.5f);
				}
			}
		}
	}
}