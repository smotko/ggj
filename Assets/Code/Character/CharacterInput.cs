﻿using UnityEngine;
using System.Collections;

public class CharacterInput : MonoBehaviour {
	public Vector2 inputDirection;
	public bool jump;
	public bool use;
	public float useTimer = 0;
	public float WAIT_TIME = 0.01f;
	private CharacterController characterController;
	CreateTiles createTiles;
	// Use this for initialization
	void Start () {
		createTiles = GameObject.Find ("TileCreator").GetComponent<CreateTiles>();
		characterController = GetComponent<CharacterController>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		inputDirection = new Vector2(Input.GetAxis("Horizontal"),Input.GetAxis("Vertical"));
		jump = Input.GetButtonDown("Fire1");

		if (useTimer == 0) {
			use = Input.GetButtonUp("Jump");
			if (use)
				useTimer+=0.01f;
		}
		else
			use=false;

		if (useTimer > 0)
		{
			useTimer+=Time.deltaTime;
			if (useTimer > WAIT_TIME)
				useTimer=0;
		}
		characterController.HandleInput(inputDirection,jump,use);
		if(Input.GetButton ("Fire3"))
		{

			createTiles.nextSet();
		}

	}
}
