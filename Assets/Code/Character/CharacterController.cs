﻿using UnityEngine;
using System.Collections;

public class CharacterController : MonoBehaviour {

	public float MAX_SPEED = 0.2f;
	public float ACC_SPEED = 2;
	public float JUMP_SPEED = 0.5f;
	public float DRAG_AMOUNT = 3;
	public float JUMP_FLAG_TIME=0.4f;
	public Vector2 speed = Vector2.zero;
	Vector2 acceleration = Vector2.zero;
	public bool dead = false;
	public Animator animator;

	public AudioClip SoundJump;
	public AudioClip SoundWalk;

	bool grounded = true; //character on ground

	bool jumpFlag = false;
	float jumpFlagTime;

	bool drag=false;
	Transform oldMan;
	bool chatActive=false;

	// Use this for initialization
	void Start () 
	{
		oldMan = GameObject.Find ("OldMan").transform;
		animator=transform.GetChild (0).GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		AudioSource src = transform.GetComponent<AudioSource>();

		if( !src.isPlaying && Mathf.Abs(rigidbody2D.velocity.x) > 0.25f )
			src.Play();
		else
		if( src.isPlaying && Mathf.Abs(rigidbody2D.velocity.x) < 0.25f )
			src.Stop();

	}
	bool IsGrounded() {
		return Physics2D.Linecast((Vector2)transform.position, (Vector2)transform.position + new Vector2(0,-0.02f), (1 << LayerMask.NameToLayer("Ground"))| (1 << LayerMask.NameToLayer("Enemies")));
	}

	public void HandleInput(Vector2 movementDirection, bool jump, bool use)
	{
		if (dead)
			return;
		if (chatActive){
			jump=false;
			movementDirection=Vector2.zero;
		}
		float t = Time.deltaTime;

		drag=false;

		//JUMP FLAG
		if (jumpFlag){
			jumpFlagTime -= t;
			if (jumpFlagTime < 0)
				jumpFlag=false;
		}
		if (movementDirection.x > 0)
			transform.localScale = Vector2.one;
		else if (movementDirection.x < 0)
			transform.localScale = new Vector2(-1,1);
		rigidbody2D.AddForce(new Vector2(movementDirection.x * ACC_SPEED,0));
		if (Mathf.Abs (rigidbody2D.velocity.x) < 0.21f)
			rigidbody2D.velocity = new Vector2(0,rigidbody2D.velocity.y);
		if (Mathf.Abs (movementDirection.x) == 0 && Mathf.Abs(rigidbody2D.velocity.x) > 0) {
			rigidbody2D.AddForce (new Vector2(-(rigidbody2D.velocity.x/Mathf.Abs(rigidbody2D.velocity.x)*DRAG_AMOUNT),0));
		}

		bool isGrounded = IsGrounded ();
		if (isGrounded) {
			animator.SetInteger("jump", 0);
		}
		if ((jump && isGrounded) || (jumpFlag && isGrounded)) {
			animator.SetInteger("jump", 1 );
			rigidbody2D.velocity= new Vector2(rigidbody2D.velocity.x,rigidbody2D.velocity.y+JUMP_SPEED);
			grounded=false;
			
			Camera.main.audio.PlayOneShot(SoundJump);
		}
		else if (jump && !isGrounded) {
			jumpFlagTime = JUMP_FLAG_TIME;
			jumpFlag=true;
		}

		//Cap speed
		rigidbody2D.velocity = new Vector2(Mathf.Clamp(rigidbody2D.velocity.x,-MAX_SPEED,MAX_SPEED),rigidbody2D.velocity.y);


		animator.SetFloat("speed", Mathf.Abs (rigidbody2D.velocity.x));


		animator.SetFloat("speedy", rigidbody2D.velocity.y);
		//USE
		if(!chatActive && use && Vector2.Distance (transform.position, oldMan.position) < 5){
			oldMan.GetComponent<OldManController>().Activate ("I'm awesome!%Booze for everyone");
			chatActive=true;
		}
		else if (chatActive && use )
			if (oldMan.GetComponent<OldManController>().Talk())
				chatActive=false;
	}

	public void OnCollisionEnter2D  (Collision2D c) {
		if (c.gameObject.tag =="Deadly") {
		    if (!dead)
				transform.RotateAround(transform.position, Vector3.forward, 90);
			dead=true;
		}
	}
	public void OnTriggerEnter2D  (Collider2D c) {
		Debug.Log (c.gameObject.name);
		if (c.gameObject.tag =="Deadly") {
			if (!dead)
				transform.RotateAround(transform.position, Vector3.forward, 90);
			dead=true;
		}
	}
}
