﻿using UnityEngine;
using System.Collections;

public class FaderScript : MonoBehaviour {

	public enum EState {
		sNone,
		sOut,
		sGoingIn,
		sIn,
		sGoingOut,
	};

	public EState State=EState.sNone;
	public float FadeOutSpeed=0.5f;
	public float FadeInSpeed=0.5f;

	private bool loadmaponfadeout = false;


	// Use this for initialization
	void Start () 
	{
		renderer.sortingLayerName = "Foreground";

		renderer.enabled = true;
		if(State == EState.sNone)
		{
			SetState(EState.sOut);
			SetState(EState.sGoingIn);
		}	
	}


	public void LoadMapOnFadeOut()
	{
		loadmaponfadeout = true;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if( State == EState.sGoingIn )
		{
			Color clr = renderer.material.GetColor("_Color");
			clr.a -= FadeInSpeed * Time.deltaTime;

			if(clr.a < 0f ) SetState ( EState.sIn );
			renderer.material.SetColor("_Color", clr);
		}

		if( State == EState.sGoingOut )
		{
			Color clr = renderer.material.GetColor("_Color");
			clr.a += FadeOutSpeed * Time.deltaTime;
			
			if(clr.a > 1f ) SetState ( EState.sOut );
			renderer.material.SetColor("_Color", clr);
		}
	}

	public void SetState(EState state)
	{
		State = state;

		if(State == EState.sOut )
		{
			renderer.material.SetColor("_Color", new Color(0f,0f,0f,1f));
			if( loadmaponfadeout == true )
			{
				Application.LoadLevel("TestScene");
				SetState(EState.sGoingIn);
			}
		}
		else
		if(State == EState.sIn )
			renderer.material.SetColor("_Color", new Color(0f,0f,0f,0f));
			//renderer.material.GetColor("_Color");
	}
}
