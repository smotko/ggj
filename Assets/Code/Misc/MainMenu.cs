﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {

	public TextMesh StartText;
	public FaderScript Fader;	
	private float tmrBeforeStart=3f;
	private bool goingup=true;
	private bool domenuupdate=true;

	// Use this for initialization
	void Start () 
	{
		StartText.color = new Color(1f,1f,1f,0f);
	}

	void CheckInput()
	{
		bool startpresssed  = Input.GetButtonUp("Start");

		if(startpresssed == true)
		{
			StartText.color = new Color(1f,1f,1f,0f);
			Fader.SetState(FaderScript.EState.sGoingOut);
			Fader.LoadMapOnFadeOut();
			domenuupdate = false;
		}

		if(Input.GetButtonUp("Jump") == true)
		{
			Fader.SetState(FaderScript.EState.sIn);
			tmrBeforeStart=0f;
		}
	}

	// Update is called once per frame
	void Update () 
	{
		CheckInput();

		if(tmrBeforeStart > 0f) tmrBeforeStart -= Time.deltaTime;

		if(domenuupdate == false ||  tmrBeforeStart > 0f ) return;

		Color clr = StartText.color;
		if(goingup == true)
		{
			if(clr.a < 1f)
			{
				clr.a += 3f * Time.deltaTime;
				if(clr.a >= 1f)
				{
					clr.a = 1f;
					goingup=false;
				}
			}
		}
		else
		if(goingup == false)
		{
			if(clr.a > 0f)
			{
				clr.a -= 3f * Time.deltaTime;
				if(clr.a <= 0f)
				{
					clr.a = 0f;
					goingup=true;
				}
			}
		}

		StartText.color = clr;


	}
}
